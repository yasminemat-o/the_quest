# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 09:12:24 2024

@author: mmazaud
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<link rel="stylesheet"  href="Styles/style.css">
	<meta charset="UTF_8">
	<title> The Quest </title>
	
</head>
<body class="body1">
	<header>
		<h1>Bienvenue sur The Quest</h1>
	</header>
	
	
    <a href="Quetes2.py">
		<img src="images/quete.png" id="image" alt="Une image de Quêtes" class="image_quete"/>
    </a>
   
	
		<img src="images/questionnaire2.png" id="image" alt="Une image de Quiz" class="image_quiz"/>
    </a>
    <a href="RèglesChifoumi.py">
		<img src="images/pierre_feuille_gazo.png" id="image" alt="Une image du jeu pierre papier gazo" class="image_chifoumi"/>
    </a>
	
</body>
</html>"""

print(html)
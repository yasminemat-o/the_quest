# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 09:12:24 2024

@author: mmazaud
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<link rel="stylesheet"  href="Styles/style.css">
	<meta charset="UTF_8">
	<title> Quiz </title>
	
</head>
<body class="body2" >
<h2>9.Qui a écrit cette punchline « J'fais confiance qu'en mon Desert Eagle, et en Zizou dans les arrêts de jeu » ? </h2>
<a class="bouton" href="Vrai9.py">Kaaris</a> </br></br>
<a class="bouton" href="Faux9.py">Booba</a> </br></br>
<a class="bouton" href="Faux9.py">Rim’k</a> </br></br>
<a class="bouton" href="Faux9.py">NTM</a> </br></br>

</body>
</html>"""

print(html)
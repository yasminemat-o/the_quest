# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 09:12:24 2024

@author: mmazaud
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<link rel="stylesheet"  href="Styles/style.css">
	<meta charset="UTF_8">
	<title> Quêtes </title>
	
</head>
<body class="body2">
<p class="quete">Bravo ! Vous avez affectué toutes les quêtes !<br/>
Voici une récompense:</p>
<img src="images/bravo.png" id="image" alt="Une image de cd"/>
<p> Un disque d'or!<p/>
</body>
</html>"""

print(html)
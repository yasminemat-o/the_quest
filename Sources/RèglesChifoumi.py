# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 09:12:24 2024

@author: mmazaud
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<link rel="stylesheet"  href="Styles/style.css">
	<meta charset="UTF_8">
	<title> Règles Pierre Papier Gazo </title>
	
</head>
<body class="body2">
	<p> Ce jeu ce joue en trois manches.</p>
	<p> Si vous et l'ordinateur êtes à egalité, ou que vous avez perdu, 
	<br>
	alors rejouer pour pouvoir tenter a nouveau votre chance.</p>
	<p> Si vous avez gagné, vous pouvez continuer dans l'aventure. </p>
	<p> Les règles sont simple: gazo bat la feuille, la feuille bat la pierre et gazo se fait battre par la pierre.  </p>
	<a href="Chifoumi.py">
		<img src="images/jouer.png" id="image" alt="Une image de bouton play" class="image_jouer"/>
    </a>
</body>
</html>"""

print(html)
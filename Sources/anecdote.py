# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 09:12:24 2024

@author: mmazaud
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<link rel="stylesheet"  href="Styles/style.css">
	<meta charset="UTF_8">
	<title> Quiz </title>
	
</head>
<body class="body2" >
<h2> Le saviez vous ? </h2>
<p>À ses débuts dans la musique il portait le pseudo Bramsou. Gazo affirmera bien plus tard que ce blaze ne lui a pas ouvert les portes.</p>
<p>« Il y avait Bramsito, Brvmsoo La Deb et moi qui m'appelais Bramsou, je passais pour un fake, car ils avaient plus de lumière ».</p>
<a class="bouton" href="page_accueil3.py">Finir le jeu</a>

</body>
</html>"""

print(html)
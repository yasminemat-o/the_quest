# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 09:12:24 2024

@author: mmazaud
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<link rel="stylesheet"  href="Styles/style.css">
	<meta charset="UTF_8">
	<title> Quiz </title>
	
</head>
<body class="body2" >
<h2>10.Parmi ces rappeurs francophones qui n’est pas français ? </h2>
<a class="bouton" href="Faux10.py">Niska</a> </br></br>
<a class="bouton" href="Faux10.py">Orelsan</a> </br></br>
<a class="bouton" href="Faux10.py">Lomepal</a> </br></br>
<a class="bouton" href="Vrai10.py">Damso</a> </br></br>

</body>
</html>"""

print(html)
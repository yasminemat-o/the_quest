# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 09:12:24 2024

@author: mmazaud
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<link rel="stylesheet"  href="Styles/style.css">
	<meta charset="UTF_8">
	<title> The Quest </title>
	
</head>
<body class="body1">
<h2> Bonjour et bienvenue sur The quest </h2>
<p class="p1"> The quest est un site comportant 2 jeux basés sur le rap Français. </p>
<p class="p1">Le but est de finir ces 2 jeux et de récupérer votre récompense ! </p>
<p class="p1"> Pour obtenir votre récompense, vous allez devoir cliquer sur la page des quêtes,<p/>
<p class="p1">(icon avec un fond blanc), après avoir fini tous les jeux.<p/>
<p class="p1"> Si vous voulez voir votre progression, il vous suffit de cliquer sur cette même page.<p/>
<a class="bouton" href="page_accueil.py">Bon courage !</a>

</body>
</html>"""

print(html)

# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 09:12:24 2024

@author: mmazaud
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<link rel="stylesheet"  href="Styles/style.css">
	<meta charset="UTF_8">
	<title> Quiz </title>
	
</head>
<body class="body2" >
<h1>Quiz Rap Français </h1>
<p class="p1"> Ce jeu est un Quiz qui testera votre Rap Français ! </p>
<p class="p1"> Il y aura un total de 15 questions. </p>
<p class="p1"> A vous de savoir y répondre. </p>
<a class="bouton" href="Question1.py">Continuez</a>

</body>
</html>"""

print(html)
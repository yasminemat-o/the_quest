# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 09:12:24 2024

@author: mmazaud
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<link rel="stylesheet"  href="Styles/style.css">
	<meta charset="UTF_8">
	<title> Quêtes </title>
	
</head>
<body class="quete3" >
<p class="p2">Terminer le questionnaire.</p>
<p class="p2"> Faire le pierre feuille ciseaux.<p/>
<a class="bouton" href="bravo.py">Récupérer la récompense</a>
</body>
</html>"""

print(html)
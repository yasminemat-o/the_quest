# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 09:12:24 2024

@author: mmazaud
"""

import cgi
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
	<link rel="stylesheet"  href="Styles/style.css">
	<meta charset="UTF_8">
	<title> Quiz </title>
	
</head>
<body class="body2" >
<h2>5.Quels sont les 3 titres les plus écoutés du rap Français ?</h2>
<a class="bouton" href="Faux5.py">La vie qu’on mène > La kiffance > Bande organisée</a> </br></br>
<a class="bouton" href="Vrai5.py">Bande organisée > La vie qu’on mène > La kiffance</a> </br></br>
<a class="bouton" href="Faux5.py">La kiffance >  Bande organisée  > La vie qu’on mène</a> </br></br>


</body>
</html>"""

print(html)